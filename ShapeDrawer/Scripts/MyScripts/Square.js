﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "Rectangle", "Point"], function(require, exports, rect, point) {
    var Square = (function (_super) {
        __extends(Square, _super);
        function Square(main, sec) {
            var _this = this;
            _super.call(this, main, sec);
            this.OnMove = function (d, e) {
                if (_this.Points.length == 0)
                    return;

                //alert(e.offsetX + " " + e.offsetY);
                var p = new point.Point();

                p.X = e.offsetX;
                p.Y = e.offsetY;

                var cxt = _this.SecondaryCanvas.getContext("2d");
                cxt.clearRect(0, 0, 800, 800);
                cxt.strokeRect(_this.Points[0].X, _this.Points[0].Y, p.X - _this.Points[0].X, p.X - _this.Points[0].X);
            };
            this.OnUp = function (d, e) {
                var cxt = _this.SecondaryCanvas.getContext("2d");
                cxt.clearRect(0, 0, 800, 800);

                var p = new point.Point();
                p.X = e.offsetX;
                p.Y = e.offsetY;

                _this.Points.push(p);

                if (_this.Points.length != 2) {
                    _this.Points = [];
                    return;
                }
                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.fillRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].X - _this.Points[0].X);
                }
                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();
                    _this.MainCanvasCtx.strokeRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].X - _this.Points[0].X);
                }

                _this.SaveShape();

                _this.Points = [];
            };
            this.Draw = function () {
                if (_this.Points.length != 2) {
                    _this.Points = [];
                    return;
                }
                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.fillRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].X - _this.Points[0].X);
                }
                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();
                    _this.MainCanvasCtx.strokeRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].X - _this.Points[0].X);
                }

                _this.Points = [];
            };
            this.Name = "Square";
        }
        return Square;
    })(rect.Rectangle);
    exports.Square = Square;
});
