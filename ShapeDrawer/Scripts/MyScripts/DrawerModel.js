﻿///<reference path="../typings/knockout/knockout.d.ts" />
define(["require", "exports", "Context", "Buttons", "ReqManager"], function(require, exports, context, butts, utils) {
    exports.DrawerViewModel = function (shapes, id) {
        var _this = this;

        _this.FillStyle = ko.observable(null);
        _this.StrokeStyle = ko.observable(new context.StrokeContext());
        _this.StrokeStyle.subscribe(function (val) {
            _this.Shape.StrokeStyle = val;
        });
        _this.FillStyle.subscribe(function (val) {
            _this.Shape.FillStyle = val;
        });

        _this.Shape = ko.observable();

        _this.isVisible = ko.observable(false);

        _this.fillCxtIsVisible = ko.observable(false);
        _this.strokeCxtIsVisible = ko.observable(false);

        _this.strokeIsExpanded = false;
        _this.fillIsExpanded = false;

        _this.cxtDescr = ko.observable(false);
        _this.cxtDescr.subscribe(function () {
            if (_this.cxtDescr() == false) {
                _this.fillCxtIsVisible(false);
                _this.FillStyle(null);
                _this.cxtChosen("");
                _this.StrokeStyle(new context.StrokeContext());
            } else {
            }
        });

        _this.cxtChosen = ko.observable();

        _this.SetShape = function (data, event) {
            if (_this.drawId() == 0) {
                CreateDraw();
            } else {
                //_this.
            }

            //var title = $(event.target).attr("title");
            var title = "";
            if (event.target.nodeName == "BUTTON")
                title = $(event.target).attr("title");
            else {
                var src = $(event.target).attr("src");
                title = src.substring(src.lastIndexOf("/") + 1, src.lastIndexOf("."));
            }

            _this.Shape = butts.GetShape(shapes, title);
            _this.Shape.FillStyle = _this.FillStyle();
            _this.Shape.StrokeStyle = _this.StrokeStyle();

            _this.strokeCxtIsVisible(true);

            _this.isVisible(_this.Shape.IsFillable);
        };

        var CreateDraw = function () {
            $.ajax({
                url: "http://localhost:17212/api/Draw/",
                type: "Post",
                success: function (data) {
                    _this.drawId(data);
                }
            });
        };

        _this.ChooseCxt = function (data, event) {
            if (event.target.value == "fill") {
                if (_this.FillStyle() == null)
                    _this.FillStyle(new context.Context());
                _this.StrokeStyle(null);

                _this.fillCxtIsVisible(true);
                _this.strokeCxtIsVisible(false);
            } else {
                if (_this.FillStyle() == null)
                    _this.FillStyle(new context.Context());
                if (_this.StrokeStyle() == null)
                    _this.StrokeStyle(new context.StrokeContext());

                _this.fillCxtIsVisible(true);
                _this.strokeCxtIsVisible(true);
            }
        };

        _this.StrokeExpandOptions = function (data, event) {
            if (!_this.strokeIsExpanded) {
                $(event.target).siblings("div").slideDown("slow");
                _this.strokeIsExpanded = true;
            } else {
                $(event.target).siblings("div").slideUp("slow");
                _this.strokeIsExpanded = false;
            }
        };

        _this.FillExpandOptions = function (data, event) {
            if (!_this.fillIsExpanded) {
                $(event.target).siblings("div").slideDown("slow");
                _this.fillIsExpanded = true;
            } else {
                $(event.target).siblings("div").slideUp("slow");
                _this.fillIsExpanded = false;
            }
        };

        _this.ChageStrokeLineWidth = ko.observable(1);

        _this.drawId = ko.observable(0);

        _this.drawId.subscribe(function () {
            for (var i = 0; i < shapes.length; i++) {
                shapes[i].DrawId = _this.drawId();
            }
        });

        if (id != 0) {
            _this.drawId(id);
        }

        _this.ChageStrokeLineWidth.subscribe(function (val) {
            var tmp = _this.StrokeStyle();
            if (!tmp)
                return;
            tmp.LineWidth = val;
            _this.StrokeStyle(tmp);
        });

        _this.StrokeColorPicker = $("#stroke").ColorPicker({
            onShow: function (clPck) {
                $(clPck).slideDown("slow");
                return false;
            },
            onHide: function (clPck) {
                $(clPck).slideUp("slow");
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var tmp = _this.StrokeStyle();
                if (!tmp)
                    return;
                tmp.Color = "#" + hex;
                _this.StrokeStyle(tmp);
            }
        });

        _this.FillColorPicker = $("#fill").ColorPicker({
            onShow: function (clPck) {
                $(clPck).slideDown("slow");
                return false;
            },
            onHide: function (clPck) {
                $(clPck).slideUp("slow");
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var tmp = _this.FillStyle();
                if (!tmp)
                    return;
                tmp.Color = "#" + hex;
                _this.FillStyle(tmp);
            }
        });

        _this.CreateDraw = new utils.ReqManager().CreateDraw;
    };
});
