﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "Shape", "Point"], function(require, exports, shape, point) {
    var Circle = (function (_super) {
        __extends(Circle, _super);
        function Circle(main, sec) {
            var _this = this;
            _super.call(this, main, sec);
            this.OnDown = function (d, e) {
                if (e === undefined)
                    return;

                // this.FillStyle = this.Globals.GlobalFillStyle;
                //this.StrokeStyle = this.Globals.GlobalStrokeStyle;
                var p = new point.Point();

                p.X = e.offsetX;
                p.Y = e.offsetY;

                _this.Points.push(p);
            };
            this.OnMove = function (d, e) {
                if (e === undefined)
                    return;
                if (_this.Points.length == 0)
                    return;

                var p = new point.Point();

                p.X = e.offsetX;
                p.Y = e.offsetY;

                var x = _this.Points[0].X;
                var y = _this.Points[0].Y;
                _this.Radius = Math.sqrt(Math.pow(p.X - x, 2) + Math.pow(p.Y - y, 2));

                var cxt = _this.SecondaryCanvas.getContext("2d");

                cxt.clearRect(0, 0, _this.SecondaryCanvas.width, _this.SecondaryCanvas.height);

                cxt.beginPath();
                cxt.arc(_this.Points[0].X, _this.Points[0].Y, _this.Radius, 0, 2 * Math.PI, false);
                cxt.stroke();
            };
            this.OnUp = function (d, e) {
                if (e === undefined)
                    return;
                var cxt = _this.SecondaryCanvas.getContext("2d");
                cxt.clearRect(0, 0, 800, 800);

                var p = new point.Point();
                p.X = e.offsetX;
                p.Y = e.offsetY;

                _this.Points.push(p);

                if (_this.Points.length != 2) {
                    _this.Points = [];
                    return;
                }

                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.beginPath();
                    _this.MainCanvasCtx.arc(_this.Points[0].X, _this.Points[0].Y, _this.Radius, 0, 2 * Math.PI, false);
                    _this.MainCanvasCtx.fill();
                }
                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();

                    _this.MainCanvasCtx.beginPath();
                    _this.MainCanvasCtx.arc(_this.Points[0].X, _this.Points[0].Y, _this.Radius, 0, 2 * Math.PI, false);
                    _this.MainCanvasCtx.stroke();
                }

                _this.SaveShape();

                _this.Points = [];
            };
            this.Draw = function () {
                if (_this.Points.length != 2) {
                    return;
                }

                var x = _this.Points[0].X;
                var y = _this.Points[0].Y;
                _this.Radius = Math.sqrt(Math.pow(_this.Points[1].X - x, 2) + Math.pow(_this.Points[1].Y - y, 2));

                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.beginPath();
                    _this.MainCanvasCtx.arc(_this.Points[0].X, _this.Points[0].Y, _this.Radius, 0, 2 * Math.PI, false);
                    _this.MainCanvasCtx.fill();
                }
                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();

                    _this.MainCanvasCtx.beginPath();
                    _this.MainCanvasCtx.arc(_this.Points[0].X, _this.Points[0].Y, _this.Radius, 0, 2 * Math.PI, false);
                    _this.MainCanvasCtx.stroke();
                }
            };

            this.FillStyle = null;
            this.StrokeStyle = null;

            this.Points = new Array();

            this.IsFillable = true;

            this.Name = "Circle";

            Draw:
            (function () {
            });
        }
        return Circle;
    })(shape.Shape);
    exports.Circle = Circle;
});
