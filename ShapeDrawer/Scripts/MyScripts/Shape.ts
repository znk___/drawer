﻿///<reference path="../typings/jquery/jquery.d.ts" />
import context = require("Context");
import globals = require("Globals");
import point = require("Point");

export class Shape {
    Name: string;

    Points: Array<point.Point>;

    OnUp = function (d,e) { };
    OnDown = function (d,e) { };

    OnMove = function (d,e) {
       
    };

    IsFillable: boolean;

    FillStyle: context.Context;
    StrokeStyle: context.StrokeContext;

    MainCanvas: HTMLCanvasElement;
    MainCanvasCtx: CanvasRenderingContext2D;

    SecondaryCanvas: HTMLCanvasElement;
    SecondaryCanvCtx: CanvasRenderingContext2D;

    DrawId: number;

    Draw = function () {

    };

    constructor(main : HTMLCanvasElement, sec: HTMLCanvasElement) {
        
        this.MainCanvas = main;
        this.SecondaryCanvas = sec;

        if(this.MainCanvas != null)
            this.MainCanvasCtx = this.MainCanvas.getContext("2d");

        if (this.SecondaryCanvas != null)
            this.SecondaryCanvCtx = this.SecondaryCanvas.getContext("2d");
    }

    SaveShape =  ()=> {
        var obj = {
            Points: this.Points,
            FillStyle: this.FillStyle,
            StrokeStyle: this.StrokeStyle,
            Name: this.Name,
            DrawId: this.DrawId,
        }

        $.ajax({
            url: "http://localhost:17212/api/Shape",
            type: "Post",
            data: obj

        });
    };

}

