﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function(require, exports) {
    (function (StyleType) {
        StyleType[StyleType["Color"] = 0] = "Color";
        StyleType[StyleType["Gradient"] = 1] = "Gradient";
        StyleType[StyleType["Pattern"] = 2] = "Pattern";
    })(exports.StyleType || (exports.StyleType = {}));
    var StyleType = exports.StyleType;

    var ColorPoint = (function () {
        function ColorPoint(Id) {
            this.Id = Id;
        }
        return ColorPoint;
    })();
    exports.ColorPoint = ColorPoint;

    var Context = (function () {
        function Context() {
            var _this = this;
            this.GetStyle = function () {
                switch (_this.Style) {
                    case 0 /* Color */:
                        return _this.Color;
                    case 1 /* Gradient */:
                        return _this.Gradient;
                    case 2 /* Pattern */:
                        return _this.Patern;
                }
            };
            this.Style = 0 /* Color */;
            this.Color = "#000";
        }
        return Context;
    })();
    exports.Context = Context;

    var StrokeContext = (function (_super) {
        __extends(StrokeContext, _super);
        function StrokeContext() {
            _super.call(this);
            this.Color = "#f00";
            this.LineWidth = 1;
        }
        return StrokeContext;
    })(Context);
    exports.StrokeContext = StrokeContext;
});
