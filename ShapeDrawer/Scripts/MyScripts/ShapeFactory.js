﻿///<reference path="../typings/jquery/jquery.d.ts" />
///<reference path="../typings/knockout/knockout.d.ts" />
define(["require", "exports", "Rectangle", "Square", "Circle", "Context", "Point"], function(require, exports, rect, sq, circ, context, point) {
    var ShapeFactory = (function () {
        function ShapeFactory(canv) {
            var _this = this;
            this.Draw = function (sh) {
                var obj = _this.FindShape(sh.Name);

                obj.FillStyle = _this.SetFillStyle(sh);
                obj.StrokeStyle = _this.SetStrokeStyle(sh);
                obj.Points = _this.SetPoints(sh);

                obj.Draw();
            };
            this.FindShape = function (name) {
                for (var i = 0; i < _this.Shapes.length; i++) {
                    if (_this.Shapes[i].Name == name)
                        return _this.Shapes[i];
                }
                return null;
            };
            this.SetFillStyle = function (sh) {
                if (sh.FillStyle.Color != null || sh.FillStyle.Gradient != null || sh.FillStyle.Pattern != null) {
                    var fill = new context.Context();
                    fill.Color = sh.FillStyle.Color;
                    fill.Gradient = sh.FillStyle.Gradient;
                    fill.Patern = sh.FillStyle.Pattern;
                    return fill;
                }

                return null;
            };
            this.SetStrokeStyle = function (sh) {
                if (sh.StrokeStyle.Color != null || sh.StrokeStyle.Gradient != null || sh.StrokeStyle.Pattern != null) {
                    var fill = new context.StrokeContext();
                    fill.Color = sh.StrokeStyle.Color;
                    fill.Gradient = sh.StrokeStyle.Gradient;
                    fill.Patern = sh.StrokeStyle.Pattern;
                    fill.LineWidth = sh.StrokeStyle.LineWidth;

                    return fill;
                }

                return null;
            };
            this.SetPoints = function (sh) {
                var points = new Array();

                for (var i = 0; i < sh.Points.length; i++) {
                    var pt = new point.Point();
                    pt.X = sh.Points[i].X;
                    pt.Y = sh.Points[i].Y;
                    points.push(pt);
                }

                return points;
            };
            this.Shapes = new Array();

            this.Shapes.push(new rect.Rectangle(canv, null));
            this.Shapes.push(new sq.Square(canv, null));
            this.Shapes.push(new circ.Circle(canv, null));
        }
        return ShapeFactory;
    })();
    exports.ShapeFactory = ShapeFactory;
});
