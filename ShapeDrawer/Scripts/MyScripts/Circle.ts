﻿import shape = require("Shape");
import point = require("Point");
import globals = require("Globals");

export class Circle extends shape.Shape {
    Radius: number;
    constructor(main: HTMLCanvasElement, sec: HTMLCanvasElement) {
        super(main, sec);
      
        this.FillStyle = null;
        this.StrokeStyle = null;

        this.Points = new Array();

        this.IsFillable = true;

        this.Name = "Circle";


        Draw: () => {
        };
    }
    OnDown = (d,e) => {
        if (e === undefined) return;
       // this.FillStyle = this.Globals.GlobalFillStyle;
        //this.StrokeStyle = this.Globals.GlobalStrokeStyle;


        var p = new point.Point();

        p.X = e.offsetX;
        p.Y = e.offsetY;

        this.Points.push(p);

    }

    OnMove = (d,e) => {
        if (e === undefined) return;
        if (this.Points.length == 0) return;

        var p = new point.Point();

        p.X = e.offsetX;
        p.Y = e.offsetY;

        var x = this.Points[0].X;
        var y = this.Points[0].Y;
        this.Radius = Math.sqrt(Math.pow(p.X - x, 2) + Math.pow(p.Y - y, 2));

        var cxt = this.SecondaryCanvas.getContext("2d");

        cxt.clearRect(0, 0, this.SecondaryCanvas.width, this.SecondaryCanvas.height);

        cxt.beginPath();
        cxt.arc(this.Points[0].X, this.Points[0].Y, this.Radius, 0, 2 * Math.PI, false);
        cxt.stroke();
    }

    OnUp = (d,e) => {
        if (e === undefined) return;
        var cxt = this.SecondaryCanvas.getContext("2d");
        cxt.clearRect(0, 0, 800, 800);

        var p = new point.Point();
        p.X = e.offsetX;
        p.Y = e.offsetY;

        this.Points.push(p);

        if (this.Points.length != 2) {
            this.Points = [];
            return;
        }

        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.beginPath();
            this.MainCanvasCtx.arc(this.Points[0].X, this.Points[0].Y, this.Radius, 0, 2 * Math.PI, false);
            this.MainCanvasCtx.fill();
        }
        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();

            this.MainCanvasCtx.beginPath();
            this.MainCanvasCtx.arc(this.Points[0].X, this.Points[0].Y, this.Radius, 0, 2 * Math.PI, false);
            this.MainCanvasCtx.stroke();
        }

        this.SaveShape();

        this.Points = [];
    }

    Draw = () => {

        if (this.Points.length != 2) {
            return;
        }

        var x = this.Points[0].X;
        var y = this.Points[0].Y;
        this.Radius = Math.sqrt(Math.pow(this.Points[1].X - x, 2) + Math.pow(this.Points[1].Y - y, 2));

        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.beginPath();
            this.MainCanvasCtx.arc(this.Points[0].X, this.Points[0].Y, this.Radius, 0, 2 * Math.PI, false);
            this.MainCanvasCtx.fill();
        }
        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();

            this.MainCanvasCtx.beginPath();
            this.MainCanvasCtx.arc(this.Points[0].X, this.Points[0].Y, this.Radius, 0, 2 * Math.PI, false);
            this.MainCanvasCtx.stroke();
        }
       
    }
}
