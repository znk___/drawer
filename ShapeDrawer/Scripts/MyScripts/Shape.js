﻿define(["require", "exports"], function(require, exports) {
    var Shape = (function () {
        function Shape(main, sec) {
            var _this = this;
            this.OnUp = function (d, e) {
            };
            this.OnDown = function (d, e) {
            };
            this.OnMove = function (d, e) {
            };
            this.Draw = function () {
            };
            this.SaveShape = function () {
                var obj = {
                    Points: _this.Points,
                    FillStyle: _this.FillStyle,
                    StrokeStyle: _this.StrokeStyle,
                    Name: _this.Name,
                    DrawId: _this.DrawId
                };

                $.ajax({
                    url: "http://localhost:17212/api/Shape",
                    type: "Post",
                    data: obj
                });
            };
            this.MainCanvas = main;
            this.SecondaryCanvas = sec;

            if (this.MainCanvas != null)
                this.MainCanvasCtx = this.MainCanvas.getContext("2d");

            if (this.SecondaryCanvas != null)
                this.SecondaryCanvCtx = this.SecondaryCanvas.getContext("2d");
        }
        return Shape;
    })();
    exports.Shape = Shape;
});
