﻿import rect = require("Rectangle");
import globals = require("Globals");
import point = require("Point");

export class Square extends rect.Rectangle {

    constructor(main: HTMLCanvasElement, sec: HTMLCanvasElement){
        super(main,sec);  
        this.Name = "Square";
    }
    OnMove = (d,e) => {
        if (this.Points.length == 0) return;


        //alert(e.offsetX + " " + e.offsetY);

        var p = new point.Point();

        p.X = e.offsetX;
        p.Y = e.offsetY;

        var cxt = this.SecondaryCanvas.getContext("2d");
        cxt.clearRect(0, 0, 800, 800);
        cxt.strokeRect(this.Points[0].X, this.Points[0].Y, p.X - this.Points[0].X, p.X - this.Points[0].X);
    }
    OnUp = (d,e) => {

        var cxt = this.SecondaryCanvas.getContext("2d");
        cxt.clearRect(0, 0, 800, 800);

            var p = new point.Point();
        p.X = e.offsetX;
        p.Y = e.offsetY;

        this.Points.push(p);

        if (this.Points.length != 2) {
            this.Points = [];
            return;
        }
        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.fillRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].X - this.Points[0].X
                );
        }
        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();
            this.MainCanvasCtx.strokeRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].X - this.Points[0].X);
        }

            this.SaveShape();

        this.Points = [];
        }

    Draw = () => {

        if (this.Points.length != 2) {
            this.Points = [];
            return;
        }
        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.fillRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].X - this.Points[0].X
                );
        }
        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();
            this.MainCanvasCtx.strokeRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].X - this.Points[0].X);
        }

        this.Points = [];
    }
}
