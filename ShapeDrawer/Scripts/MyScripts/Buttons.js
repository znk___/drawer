﻿///<reference path="../typings/jquery/jquery.d.ts" />
///<reference path="../typings/knockout/knockout.d.ts" />
define(["require", "exports", "Rectangle", "Square", "Circle", "DrawerModel", "ShapeFactory", "ReqManager"], function(require, exports, rect, sqr, circ, view, fact, utils) {
    $(document).ready(function () {
        var Shapes = new Array();
        var MainCanv = document.getElementById("canvM");
        var SecCanv = document.getElementById("canvS");

        InitShapes(Shapes, MainCanv, SecCanv);
        InitButtons(Shapes);

        var drId = $("#drawId").val() !== undefined ? $("#drawId").val() : 0;

        ko.applyBindings(new view.DrawerViewModel(Shapes, drId));

        if (drId > 0) {
            var factory = new fact.ShapeFactory(MainCanv);
            var util = new utils.ReqManager;

            util.GetDraw(function (data) {
                var cxt = MainCanv.getContext("2d");
                cxt.clearRect(0, 0, MainCanv.width, MainCanv.height);

                for (var i = 0; i < data.Shapes.length; i++) {
                    factory.Draw(data.Shapes[i]);
                }
            }, drId);
        }
    });

    function InitShapes(shapes, canvM, canvS) {
        shapes.push(new rect.Rectangle(canvM, canvS));
        shapes.push(new sqr.Square(canvM, canvS));
        shapes.push(new circ.Circle(canvM, canvS));
    }

    function InitButtons(shapes) {
        var tools = $(".tools");

        for (var i = 0; i < shapes.length; i++) {
            var shp = shapes[i];

            var b = $("<button  />", {
                title: shp.Name
            });
            var im = $("<img />", {
                src: "/Images/" + shp.Name + ".png",
                alt: "?"
            });

            b.attr("data-bind", "event: {click: SetShape}");
            b.append(im);
            tools.append(b);
        }
    }

    exports.GetShape = function (shapes, title) {
        for (var i = 0; shapes.length; i++) {
            if (shapes[i].Name == title)
                return shapes[i];
        }
        return null;
    };
});
