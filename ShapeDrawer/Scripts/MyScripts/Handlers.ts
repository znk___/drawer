﻿import context = require("Context");
import shape = require("Shape");

export class Handlers { 
    
    GlobalFillStyle: context.Context;
    GlobalStrokeStyle: context.StrokeContext;

    Subscribers: Array<shape.Shape>;
    constructor() { this.Subscribers = new Array();}
    //constructor(GlobalFillStyle: context.Context, GlobalStrokeStyle: context.StrokeContext) {
    //    this.GlobalFillStyle = GlobalFillStyle;
    //    this.GlobalStrokeStyle = GlobalStrokeStyle;
    //    this.Subscribers = new Array();
    //}

   

    ChooseShapeType = (shp: shape.Shape) => {
        if (this.GlobalStrokeStyle == null) {
            this.GlobalStrokeStyle = new context.StrokeContext();
        }

        if (shp.IsFillable) {
            $("#shapeType").css("display", "block");
            $("#strokeOpts").css("display", "block");
        }
        else {
            $("#shapeType").css("display", "none");
            $("#strokeOpts").css("display", "block");
        }

        this.OnChangeContext();
    }

    OnShapeTypeChangeClick = (e)=> {
        if ($(e.target).is(":checked")) {
            $("#fillType").css("display", "block");

        }
        else {
            $("#fillType").css("display", "none");
            $("#fillOpts").css("display", "none");
            $("#storkeOpts").css("display", "block");
            //this.GlobalFillStyle = null;
        }
    }

    OnFillTypeChageClick = (e)=> {
        if (e.target.value == "fill") {
            $("#strokeOpts").css("display", "none");
            $("#fillOpts").css("display", "block");

            if (this.GlobalFillStyle == null)
                this.GlobalFillStyle = new context.Context();
            this.GlobalStrokeStyle = null;
        }
        else if (e.target.value == "both") {
            $("#fillOpts").css("display", "block");
            $("#strokeOpts").css("display", "block");

            if (this.GlobalFillStyle == null)
                this.GlobalFillStyle = new context.Context();
            if (this.GlobalStrokeStyle == null)
                this.GlobalStrokeStyle = new context.StrokeContext();
        }
        this.OnChangeContext();
    }


    ShowStrokeOptionsClick =  (e)=> {
        var button = $(e.target);
        var div = button.siblings('div');
        if (div.css("display") == "none") {
            div.slideDown();
        }
        else {
            div.slideUp();
        }
    }

    StrokeStyleLineWidthChange = (e) => {
        this.OnChangeLineWidth($(e.target).val());
 }

    private OnChangeContext() {
        for (var i = 0; i < this.Subscribers.length; i++) {
            this.Subscribers[i].FillStyle = this.GlobalFillStyle;
            this.Subscribers[i].StrokeStyle = this.GlobalStrokeStyle;
        }
    }

    public OnChangeFillColor(color:string) {
      for (var i = 0; i < this.Subscribers.length; i++) {
            this.Subscribers[i].FillStyle.Color = color;
        }
    }

    public OnChageStrokeColor(color: string) {
        for (var i = 0; i < this.Subscribers.length; i++) {
            this.Subscribers[i].StrokeStyle.Color = color;
        }
    }

    public OnChangeLineWidth(width: number) {
        for (var i = 0; i < this.Subscribers.length; i++) {
            this.Subscribers[i].StrokeStyle.LineWidth = width;
        }
    }

}

