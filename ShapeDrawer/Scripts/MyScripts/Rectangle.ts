﻿import shape = require("Shape");
import globals = require("Globals");
import point = require("Point");

export class Rectangle extends shape.Shape {


    constructor(main: HTMLCanvasElement, sec: HTMLCanvasElement) {
        super(main,sec);
        
        

        this.FillStyle = null;
        this.StrokeStyle = null;

        this.Points = new Array();

        this.IsFillable = true;

        this.Name = "Rectangle";
    }

    Draw = () => {
        if (this.Points.length != 2) {
            this.Points = [];
            return;
        }

        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.fillRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].Y - this.Points[0].Y
                );
        }

        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();
            this.MainCanvasCtx.strokeRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].Y - this.Points[0].Y);
        }
    };
    OnUp = (d,e) => {
        if (e === undefined) return;
        var cxt = this.SecondaryCanvas.getContext("2d");
        cxt.clearRect(0, 0, 800, 800);

        var p = new point.Point();
        p.X = e.offsetX;
        p.Y = e.offsetY;

        this.Points.push(p);

        if (this.Points.length != 2) {
            this.Points = [];
            return;
        }
        if (this.FillStyle != null) {
            this.MainCanvasCtx.fillStyle = this.FillStyle.GetStyle();
            this.MainCanvasCtx.fillRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].Y - this.Points[0].Y
                );
        }
        if (this.StrokeStyle != null) {
            this.MainCanvasCtx.lineWidth = this.StrokeStyle.LineWidth;
            this.MainCanvasCtx.strokeStyle = this.StrokeStyle.GetStyle();
            this.MainCanvasCtx.strokeRect(
                this.Points[0].X,
                this.Points[0].Y,
                this.Points[1].X - this.Points[0].X,
                this.Points[1].Y - this.Points[0].Y);
        }

        this.SaveShape();

        this.Points = [];
    }

    OnDown = (d,e) => {
        if (e === undefined) return;
        //this.FillStyle = this.Globals.GlobalFillStyle;
        //this.StrokeStyle = this.Globals.GlobalStrokeStyle;


        var p = new point.Point();

        p.X = e.offsetX;
        p.Y = e.offsetY;

        this.Points.push(p);

    }

        public OnMove = (d,e) => {
        if (e === undefined) return;

         //   console.log(e.offsetX);
          //  console.log(e.offsetY);

        if (this.Points.length == 0) return;

            var p = new point.Point();

        p.X = e.offsetX;
        p.Y = e.offsetY;

        var cxt = this.SecondaryCanvas.getContext("2d");
        cxt.clearRect(0, 0, 800, 800);
        cxt.strokeRect(this.Points[0].X, this.Points[0].Y, p.X - this.Points[0].X, p.Y - this.Points[0].Y);
    }
}
