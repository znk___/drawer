﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "Shape", "Point"], function(require, exports, shape, point) {
    var Rectangle = (function (_super) {
        __extends(Rectangle, _super);
        function Rectangle(main, sec) {
            var _this = this;
            _super.call(this, main, sec);
            this.Draw = function () {
                if (_this.Points.length != 2) {
                    _this.Points = [];
                    return;
                }

                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.fillRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].Y - _this.Points[0].Y);
                }

                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();
                    _this.MainCanvasCtx.strokeRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].Y - _this.Points[0].Y);
                }
            };
            this.OnUp = function (d, e) {
                if (e === undefined)
                    return;
                var cxt = _this.SecondaryCanvas.getContext("2d");
                cxt.clearRect(0, 0, 800, 800);

                var p = new point.Point();
                p.X = e.offsetX;
                p.Y = e.offsetY;

                _this.Points.push(p);

                if (_this.Points.length != 2) {
                    _this.Points = [];
                    return;
                }
                if (_this.FillStyle != null) {
                    _this.MainCanvasCtx.fillStyle = _this.FillStyle.GetStyle();
                    _this.MainCanvasCtx.fillRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].Y - _this.Points[0].Y);
                }   
                if (_this.StrokeStyle != null) {
                    _this.MainCanvasCtx.lineWidth = _this.StrokeStyle.LineWidth;
                    _this.MainCanvasCtx.strokeStyle = _this.StrokeStyle.GetStyle();
                    _this.MainCanvasCtx.strokeRect(_this.Points[0].X, _this.Points[0].Y, _this.Points[1].X - _this.Points[0].X, _this.Points[1].Y - _this.Points[0].Y);
                }

                _this.SaveShape();

                _this.Points = [];
            };
            this.OnDown = function (d, e) {
                if (e === undefined)
                    return;

                //this.FillStyle = this.Globals.GlobalFillStyle;
                //this.StrokeStyle = this.Globals.GlobalStrokeStyle;
                var p = new point.Point();

                p.X = e.offsetX;
                p.Y = e.offsetY;

                _this.Points.push(p);
            };
            this.OnMove = function (d, e) {
                if (e === undefined)
                    return;

                //   console.log(e.offsetX);
                //  console.log(e.offsetY);
                if (_this.Points.length == 0)
                    return;

                var p = new point.Point();

                p.X = e.offsetX;
                p.Y = e.offsetY;

                var cxt = _this.SecondaryCanvas.getContext("2d");
                cxt.clearRect(0, 0, 800, 800);
                cxt.strokeRect(_this.Points[0].X, _this.Points[0].Y, p.X - _this.Points[0].X, p.Y - _this.Points[0].Y);
            };

            this.FillStyle = null;
            this.StrokeStyle = null;

            this.Points = new Array();

            this.IsFillable = true;

            this.Name = "Rectangle";
        }
        return Rectangle;
    })(shape.Shape);
    exports.Rectangle = Rectangle;
});
