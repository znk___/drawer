﻿import context = require("Context");
import shape = require("Shape");
import handlers = require("Handlers");


export class GLobal{
    
     GlobalFillStyle: context.Context;
     GlobalStrokeStyle: context.StrokeContext;

     GlobalMainCanv: HTMLCanvasElement;
    GlobalSecCanv: HTMLCanvasElement;
     GlobalMainCanvCtx: CanvasRenderingContext2D;
    GlobalSecCanvCtx: CanvasRenderingContext2D;

     Shapes: Array<shape.Shape>;

    Handlers: handlers.Handlers;
}

