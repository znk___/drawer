﻿///<reference path="../typings/knockout/knockout.d.ts" />
define(["require", "exports", "ShapeFactory", "ReqManager"], function(require, exports, fact, utils) {
    $(document).ready(function () {
        var util = new utils.ReqManager();
        util.GetDraws(function (items) {
            ko.applyBindings(new DrawsViewModel(items));
        });
    });

    var DrawsViewModel = function (items) {
        var _this = this;

        _this.utils = new utils.ReqManager();

        _this.canv = document.getElementById("canv");
        _this.cxt = _this.canv.getContext("2d");
        _this.factory = new fact.ShapeFactory(_this.canv);

        _this.CreateDraw = _this.utils.CreateDraw;

        _this.draws = ko.observableArray(items);
        _this.selectedDraw = ko.observable();
        _this.selectedDraw.subscribe(function () {
            if (_this.selectedDraw() === undefined) {
                return;
            }

            _this.utils.GetDraw(function (data) {
                _this.cxt.clearRect(0, 0, _this.canv.width, _this.canv.height);

                for (var i = 0; i < data.Shapes.length; i++) {
                    _this.factory.Draw(data.Shapes[i]);
                }
            }, _this.selectedDraw());
        });

        _this.EditDraw = function () {
            location.href = "http://localhost:17212/Home/Index/" + _this.selectedDraw();
        };

        _this.DeleteDraw = function () {
            $.ajax({
                url: "http://localhost:17212/api/Draw/" + _this.selectedDraw(),
                type: "Delete",
                success: function (data) {
                    if (data == false)
                        return;

                    var item = _this.selectedDraw();
                    var index = _this.draws.indexOf(item);
                    _this.draws.splice(index, 1);
                    _this.selectedDraw(undefined);
                    _this.cxt.clearRect(0, 0, _this.canv.width, _this.canv.height);
                }
            });
        };
    };
});
