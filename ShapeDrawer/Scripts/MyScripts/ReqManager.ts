﻿///<reference path="../typings/jquery/jquery.d.ts" />

export class ReqManager {
    GetDraws = (handler) => {
        var items = new Array();

        $.ajax({
            url: "http://localhost:17212/api/Draw",
            type: "Get",
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    items.push(data[i].Id);
                }
                // ko.applyBindings(new DrawsViewModel(items));
                handler( items);
            }
        });
    }

    GetDraw = (handler, id) => {
        $.ajax({
            url: "http://localhost:17212/api/Draw/" + id,
            type: "Get",
            success: function (data) {
                handler(data);
            }
        });
    }

    CreateDraw = () => {
       location.href = "http://localhost:17212/Home/Index";
    }
} 