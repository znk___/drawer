﻿
export enum StyleType { Color, Gradient, Pattern }

export class ColorPoint {
    Id: number;
    PointPos: number;
    Color: string;

    constructor(Id: number) {
        this.Id = Id;
    }
}

export class Context {
    public Style: StyleType;

    Gradient: any;
    Color: string;
    Patern: any;

    ColorPoints: Array<ColorPoint>;

    constructor() {
        this.Style = StyleType.Color;
        this.Color = "#000";

    }

        public GetStyle = () => {
        switch (this.Style) {
            case StyleType.Color:
                return this.Color;
            case StyleType.Gradient:
                return this.Gradient;
            case StyleType.Pattern:
                return this.Patern;
        }
    }

    }

export class StrokeContext extends Context {
    LineWidth: number;
    constructor() {
        super();
        this.Color = "#f00";
        this.LineWidth = 1;

    }

} 
