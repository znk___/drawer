﻿///<reference path="../typings/jquery/jquery.d.ts" />
define(["require", "exports"], function(require, exports) {
    var ReqManager = (function () {
        function ReqManager() {
            this.GetDraws = function (handler) {
                var items = new Array();

                $.ajax({
                    url: "http://localhost:17212/api/Draw",
                    type: "Get",
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            items.push(data[i].Id);
                        }

                        // ko.applyBindings(new DrawsViewModel(items));
                        handler(items);
                    }
                });
            };
            this.GetDraw = function (handler, id) {
                $.ajax({
                    url: "http://localhost:17212/api/Draw/" + id,
                    type: "Get",
                    success: function (data) {
                        handler(data);
                    }
                });
            };
            this.CreateDraw = function () {
                location.href = "http://localhost:17212/Home/Index";
            };
        }
        return ReqManager;
    })();
    exports.ReqManager = ReqManager;
});
