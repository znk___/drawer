﻿///<reference path="../typings/jquery/jquery.d.ts" />
///<reference path="../typings/knockout/knockout.d.ts" />

import rect = require("Rectangle");
import sq = require("Square");
import circ = require("Circle");
import shape = require("Shape");
import context = require("Context");
import point = require("Point");

export class ShapeFactory { 
    Shapes: Array <shape.Shape>;

    constructor(canv: HTMLCanvasElement) {
        this.Shapes = new Array();

        this.Shapes.push(new rect.Rectangle(canv, null));
        this.Shapes.push(new sq.Square(canv, null));
        this.Shapes.push(new circ.Circle(canv, null));

    }

     Draw =  (sh) =>{
         var obj = this.FindShape(sh.Name);

         obj.FillStyle = this.SetFillStyle(sh);
         obj.StrokeStyle = this.SetStrokeStyle(sh);
         obj.Points = this.SetPoints(sh);

         obj.Draw();

    }

     FindShape = (name: string) =>{
        for (var i = 0; i < this.Shapes.length; i++) {
            if (this.Shapes[i].Name == name)
                return this.Shapes[i];
        }
        return null;
     }

    SetFillStyle = (sh) =>{
        if (sh.FillStyle.Color != null || sh.FillStyle.Gradient != null || sh.FillStyle.Pattern != null)
        {
            var fill = new context.Context();
            fill.Color = sh.FillStyle.Color;
            fill.Gradient = sh.FillStyle.Gradient;
            fill.Patern = sh.FillStyle.Pattern;
            return fill;
        }

        return null;
    }

    SetStrokeStyle = (sh) => {
        if (sh.StrokeStyle.Color != null || sh.StrokeStyle.Gradient != null || sh.StrokeStyle.Pattern != null) {
            var fill = new context.StrokeContext();
            fill.Color = sh.StrokeStyle.Color;
            fill.Gradient = sh.StrokeStyle.Gradient;
            fill.Patern = sh.StrokeStyle.Pattern;
            fill.LineWidth = sh.StrokeStyle.LineWidth;

            return fill;
        }

        return null;
    }

    SetPoints = (sh) => {
        var points = new Array<point.Point>();

        for (var i = 0; i < sh.Points.length; i++) {
            var pt = new point.Point();
            pt.X = sh.Points[i].X;
            pt.Y = sh.Points[i].Y;
            points.push(pt);
        }

        return points;
    }

} 