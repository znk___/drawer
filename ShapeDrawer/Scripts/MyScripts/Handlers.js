﻿define(["require", "exports", "Context"], function(require, exports, context) {
    var Handlers = (function () {
        function Handlers() {
            var _this = this;
            //constructor(GlobalFillStyle: context.Context, GlobalStrokeStyle: context.StrokeContext) {
            //    this.GlobalFillStyle = GlobalFillStyle;
            //    this.GlobalStrokeStyle = GlobalStrokeStyle;
            //    this.Subscribers = new Array();
            //}
            this.ChooseShapeType = function (shp) {
                if (_this.GlobalStrokeStyle == null) {
                    _this.GlobalStrokeStyle = new context.StrokeContext();
                }

                if (shp.IsFillable) {
                    $("#shapeType").css("display", "block");
                    $("#strokeOpts").css("display", "block");
                } else {
                    $("#shapeType").css("display", "none");
                    $("#strokeOpts").css("display", "block");
                }

                _this.OnChangeContext();
            };
            this.OnShapeTypeChangeClick = function (e) {
                if ($(e.target).is(":checked")) {
                    $("#fillType").css("display", "block");
                } else {
                    $("#fillType").css("display", "none");
                    $("#fillOpts").css("display", "none");
                    $("#storkeOpts").css("display", "block");
                    //this.GlobalFillStyle = null;
                }
            };
            this.OnFillTypeChageClick = function (e) {
                if (e.target.value == "fill") {
                    $("#strokeOpts").css("display", "none");
                    $("#fillOpts").css("display", "block");

                    if (_this.GlobalFillStyle == null)
                        _this.GlobalFillStyle = new context.Context();
                    _this.GlobalStrokeStyle = null;
                } else if (e.target.value == "both") {
                    $("#fillOpts").css("display", "block");
                    $("#strokeOpts").css("display", "block");

                    if (_this.GlobalFillStyle == null)
                        _this.GlobalFillStyle = new context.Context();
                    if (_this.GlobalStrokeStyle == null)
                        _this.GlobalStrokeStyle = new context.StrokeContext();
                }
                _this.OnChangeContext();
            };
            this.ShowStrokeOptionsClick = function (e) {
                var button = $(e.target);
                var div = button.siblings('div');
                if (div.css("display") == "none") {
                    div.slideDown();
                } else {
                    div.slideUp();
                }
            };
            this.StrokeStyleLineWidthChange = function (e) {
                _this.OnChangeLineWidth($(e.target).val());
            };
            this.Subscribers = new Array();
        }
        Handlers.prototype.OnChangeContext = function () {
            for (var i = 0; i < this.Subscribers.length; i++) {
                this.Subscribers[i].FillStyle = this.GlobalFillStyle;
                this.Subscribers[i].StrokeStyle = this.GlobalStrokeStyle;
            }
        };

        Handlers.prototype.OnChangeFillColor = function (color) {
            for (var i = 0; i < this.Subscribers.length; i++) {
                this.Subscribers[i].FillStyle.Color = color;
            }
        };

        Handlers.prototype.OnChageStrokeColor = function (color) {
            for (var i = 0; i < this.Subscribers.length; i++) {
                this.Subscribers[i].StrokeStyle.Color = color;
            }
        };

        Handlers.prototype.OnChangeLineWidth = function (width) {
            for (var i = 0; i < this.Subscribers.length; i++) {
                this.Subscribers[i].StrokeStyle.LineWidth = width;
            }
        };
        return Handlers;
    })();
    exports.Handlers = Handlers;
});
