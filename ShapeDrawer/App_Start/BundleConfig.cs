﻿using System.Web;
using System.Web.Optimization;

namespace ShapeDrawer
{
    public class BundleConfig
    {
        // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/colorpicker").Include(
                        "~/Scripts/Myscripts/js/colorpicker.js",
                        "~/Scripts/Myscripts/js/eye.js",
                        "~/Scripts/Myscripts/js/layout.js",
                        "~/Scripts/Myscripts/js/utils.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/Drawer").Include(
                
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"
                        ));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/Content/css").Include("~/Content/main.css"));


            bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/MyScripts/knockout-3.1.0.debug.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/site.css",
                "~/Content/Main.css"
                ));

            bundles.Add(new StyleBundle("~/Content/colorpicker").Include(
                "~/Scripts/Myscripts/css/layout.css",
                "~/Scripts/Myscripts/css/colorpicker.css"
                ));

            bundles.Add(new StyleBundle("~/Content/jqueryui").Include(
                "~/Content/jquery-ui-1.10.4.custom.css"
                ));

            bundles.Add(new StyleBundle("~/Content/userdraws").Include(
                "~/Content/UserDraws.css"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}