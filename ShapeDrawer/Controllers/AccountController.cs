﻿using ShapeDrawer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text;

namespace ShapeDrawer.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        DrawerDbContext db = new DrawerDbContext();


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginUserModel user)
        {

           
                
                var users = db.Users;

                byte[] arr = System.Text.UTF8Encoding.UTF8.GetBytes(user.Password);
                var pass = Convert.ToBase64String(arr);

                int cnt = users.Count(us => us.Login == user.Login && us.Password == pass);
              
                if (cnt == 0)
                {
                    ViewBag.Msg = "Wrong input";
                    return View();
                }
                else {
                    FormsAuthentication.SetAuthCookie(user.Login, user.RememberMe);
                    return RedirectToAction("Index", "Home");
                }

            
        }

        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");

        }

        public ActionResult Register() {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterUserModel us) {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else {
                User user = null;
                user = db.Users.FirstOrDefault(u => u.Login == us.Login);
                if (user != null)
                {
                    ViewBag.Msg = "User already exists";
                    return View();
                }

                user = new User();
                user.Login = us.Login;
                byte[] arr = System.Text.UTF8Encoding.UTF8.GetBytes(us.Password);
                user.Password = Convert.ToBase64String(arr);
                using (db = new DrawerDbContext()) {
                    db.Users.Add(user);
                    db.SaveChanges();
                }
                FormsAuthentication.SetAuthCookie(user.Login, false);
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
