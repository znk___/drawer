﻿using Newtonsoft.Json;
using ShapeDrawer.Models;
using ShapeDrawer.Models.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace ShapeDrawer.Controllers
{
    public class ShapeController : ApiController
    {
        DrawerDbContext db = new DrawerDbContext();

        public void Post([FromBody]Shape shape){
            //db.FillStyles.Add(shape.FillStyle);
            var user = db.Users.FirstOrDefault(u => u.Login == User.Identity.Name);

            //shape.Draw.User = user;
            //shape.DrawId = 5;
            
            var draw = db.Draws.Find(shape.DrawId);

            if (draw == null)
            {
                draw = new Draw();
                draw.User = user;

                db.Draws.Add(draw);

                db.SaveChanges();
                
            }


            shape.Draw = draw;
            db.Shapes.Add(shape);

            db.SaveChanges();
        }

        [System.Web.Mvc.HttpGet]
        public IEnumerable<ShapeDTO> Get() {
            var shapes = db.Shapes.ToList();
            List<ShapeDTO> shapesDTO = new List<ShapeDTO>();
            foreach (var x in shapes) {
                shapesDTO.Add(new ShapeDTO(x));
            }

            return shapesDTO;

        }

        [System.Web.Mvc.HttpGet]
        public ShapeDTO Get(int id)
        {
            var shape = db.Shapes.FirstOrDefault(sh => sh.Id == id);
            var shp = new ShapeDTO(shape);


            return shp;

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
