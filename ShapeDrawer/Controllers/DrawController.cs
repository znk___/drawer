﻿using ShapeDrawer.Models;
using ShapeDrawer.Models.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ShapeDrawer.Controllers
{
    public class DrawController : ApiController
    {
        DrawerDbContext db = new DrawerDbContext();

        [HttpGet]
        public DrawDTO Get(int id) {
            
            var x = db.Draws.Find(id);
            var shapes = x.Shapes.ToList();
            List<ShapeDTO> shapesDTO = new List<ShapeDTO>();
          
            foreach (var z in shapes) {
                shapesDTO.Add(new ShapeDTO(z));
            }

            return new DrawDTO() { Id= x.Id, Shapes = shapesDTO};
        }

        public IEnumerable<DrawDTO> Get() {
            var drawsDTO = new List<DrawDTO>();
            
            var user = db.Users.FirstOrDefault(us => us.Login == this.User.Identity.Name);

            if (user == null) 
                return null;

            var draws = db.Draws.Where(d => d.User.Id == user.Id).ToList();
            
            foreach (var x in draws) {
                var draw = new DrawDTO() {Id = x.Id, Shapes = null };
                
                drawsDTO.Add(draw);
            }
            return drawsDTO ;
        }

        public int Post() {
            var user = db.Users.FirstOrDefault(u => u.Login == User.Identity.Name);
            
            if (user == null) { 
                return 0;
            }

            var draw = new Draw();
            draw.User = user;
            db.Draws.Add(draw);
            db.SaveChanges();

            return draw.Id;
        }

        [HttpDelete]
        public bool Delete(int id) {
            var draw = db.Draws.Find(id);
            if (draw == null)
                return false;

            foreach (var x in draw.Shapes) {
                db.FillStyles.Remove(db.FillStyles.Find(x.FillStyleId));
                db.StrokeStyles.Remove(db.StrokeStyles.Find(x.StrokeStyleId));
            }

            db.Draws.Remove(draw);
            db.SaveChanges();
                     
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
