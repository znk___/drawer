﻿using ShapeDrawer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShapeDrawer.Controllers
{
    public class HomeController : Controller
    {
        DrawerDbContext db;

        [Authorize]
        public ActionResult Index(int? id)
        {
            if (id != null) {
                ViewBag.drawId = (int)id;
            }

            return View();
        }


        

    }
}
