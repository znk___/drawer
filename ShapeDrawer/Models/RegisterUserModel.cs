﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models
{
    public class RegisterUserModel
    {
        [Required(ErrorMessage="Login is required")]
        public String Login { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage="Password is required")]
        public String Password { get; set; }

        [Display(Name="Confirm password")]
        [Compare("Password", ErrorMessage="Passwords do not match")]
        [Required]
        [DataType(DataType.Password)]
        public String Confirm { get; set; }
    }
}