﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models
{
    public class DrawerDbContext :DbContext
    {
       // public DbSet<ColorStop> ColorStops { get; set; }
        public DbSet<Point> Points{ get; set; }
       // public DbSet<Gradient> Gradients { get; set; }
        public DbSet<StrokeStyle> StrokeStyles { get; set; }
        public DbSet<FillStyle> FillStyles { get; set; }
        public DbSet<Shape> Shapes { get; set; }
        public DbSet<Draw> Draws { get; set; }
        public DbSet<User> Users { get; set; }

        public DrawerDbContext() : base("Drawer") {
         //   base.Configuration.ProxyCreationEnabled = false;
        }

        
    }
}