﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models.DTOModels
{
    public class FillStyleDTO
    {
        public String Color { get; set; }
        public String Gradient { get; set; }
        public String Pattern { get; set; }
        
        public FillStyleDTO(FillStyle fs) {
            Color = fs.Color;
            Pattern = fs.Pattern;
        }

    }
}