﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models.DTOModels
{
    public class ShapeDTO
    {
        public FillStyleDTO FillStyle{ get; set; }
        public StrokeStyleDTO StrokeStyle { get; set; }
        public List<PointDTO> Points{ get; set; }
        public String Name { get; set; }


        public ShapeDTO(Shape shape) {
            Name = shape.Name;
            FillStyle = new FillStyleDTO(shape.FillStyle);
            StrokeStyle = new StrokeStyleDTO(shape.StrokeStyle);
            Points = new List<PointDTO>();
            
            foreach (var x in shape.Points) { 
                Points.Add(new PointDTO(){X = x.X, Y = x.Y});
            }

        }
    }
}