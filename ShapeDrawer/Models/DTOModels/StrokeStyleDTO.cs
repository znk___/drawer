﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models.DTOModels
{
    public class StrokeStyleDTO
    {
        public String Color { get; set; }
        public int LineWidth { get; set; }

        public StrokeStyleDTO(StrokeStyle ss) {
            Color = ss.Color;
            LineWidth = ss.LineWidth;
        }

    }
}