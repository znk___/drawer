﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models.DTOModels
{
    public class PointDTO
    {
        public int X { get; set; }
        public int Y { get; set; }      
    }
}