﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models
{
    public class Draw
    {
        //public int Id { get; set; }

        //virtual public User User { get; set; }
         
        //  //public ICollection<User> Users { get; set; }
        //  //public Shape Shape { get; set; }
        //virtual public ICollection<Shape> Shapes { get; set; }


        // public Draw() {
        // //    Users = new List<User>();
        //    // Shape = new Shape();
        //     Shapes = new List<Shape>();
        //     User = new User();
        // }

         public Draw()
        {
            this.Shapes = new List<Shape>();
        }
    
        public int Id { get; set; }
        public int UserId { get; set; }
    
        public virtual User User { get; set; }
        public virtual ICollection<Shape> Shapes { get; set; }
    }
}