﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShapeDrawer.Models
{
    public class Point
    {
        //public int Id { get; set; }
        //public int X { get; set; }
        //public int Y { get; set; }

        // virtual public Shape Shape { get; set; }

        // public Point() {
        //     Shape = new Shape();
        // }

        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int ShapeId { get; set; }

        public virtual Shape Shape { get; set; }
    }
}