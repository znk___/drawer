﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models
{
    public class User
    {
        //public int Id { get; set; }
        //public String Login { get; set; }
        //public String Password { get; set; }
        
        //virtual public ICollection<Draw> Draws { get; set; }

        //public User() {
        //    Draws = new List<Draw>();
        //}

         public User()
        {
            this.Draws = new List<Draw>();
        }
    
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    
        public virtual ICollection<Draw> Draws { get; set; }
    }
}