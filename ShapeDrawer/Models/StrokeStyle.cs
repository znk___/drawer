﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShapeDrawer.Models
{
    public class StrokeStyle
    {
        // public int Id { get; set; }
        // public String Color { get; set; }
        // public int LineWidth { get; set; }

        //virtual public Shape Shape { get; set; }

        // public StrokeStyle() {
        //     Shape = new Shape();

        // }

          public StrokeStyle()
        {
            this.Shapes = new List<Shape>();
        }
    
        public int Id { get; set; }
        public string Color { get; set; }
        public int LineWidth { get; set; }
    
        public virtual ICollection<Shape> Shapes { get; set; }
    }
}