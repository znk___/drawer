﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShapeDrawer.Models
{
    public class FillStyle
    {
        //public int Id { get; set; }
        //public String Color { get; set; }
        //public String PicturePattern { get; set; }
    
        ////virtual public ICollection<Gradient >Gradients { get; set; }
        //virtual public Shape Shape { get; set; }

        //public FillStyle() {
        //    Shape = new Shape();
        //}

        public FillStyle()
        {
            this.Shapes = new List<Shape>();
        }
    
        public int Id { get; set; }
        public string Color { get; set; }
        public string Pattern { get; set; }
    
        public virtual ICollection<Shape> Shapes { get; set; }
    }
}