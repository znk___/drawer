﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShapeDrawer.Models
{
    public class Shape
    {
      //  public int Id { get; set; }
      //  public String Name { get; set; }
      //  public int OrderInDraw { get; set; }

      ////  public int FillStyleId { get; set; }
      //  //public int StrokeStyleId { get; set; }

      //  virtual public ICollection<FillStyle> FillStyles { get; set; }
      //   virtual public  ICollection<StrokeStyle> StrokeStyles { get; set; }
      //   virtual public  Draw Draw { get; set; }

      //   virtual public ICollection<Point> Points { get; set; }
      // // public ICollection<Draw> Draws { get; set; }

      //  public Shape() {
      //      Points = new List<Point>();
      //     // Draws = new List<Draw>();
      //      Draw = new Draw();

      //      FillStyles = new List<FillStyle>();
      //      StrokeStyles = new List<StrokeStyle>();
      //  }
        public Shape()
        {
            this.Points = new List<Point>();
        }
    
        public int Id { get; set; }
        public int DrawId { get; set; }
        public Nullable<int> FillStyleId { get; set; }
        public Nullable<int> StrokeStyleId { get; set; }
        public string Name { get; set; }
    
        public virtual Draw Draw { get; set; }
        public virtual FillStyle FillStyle { get; set; }
        public virtual ICollection<Point> Points { get; set; }
        public virtual StrokeStyle StrokeStyle { get; set; }

    }
}